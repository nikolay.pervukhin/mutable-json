# Mutable Json

Класс для обновления свойств json по пути. Чтобы иницилизовать нужно передать исходный json в конструктор класса.

```java
new MutableJson("{}")
```

Далее можно вызывать метод update с параметрами:
- путь к полю (object1.object2.parameter),
- новое значение (значение из примитивного типа),
- forceSetNull - для установки null

## Примеры

Простое изменение (добавление в случае отсутствия) элемента по пути. В данном примере мы добавляем объект 'some'  со свойством value и значением myValue.

```java
System.out.println(new MutableJson("{}").update("some.value", "myValue"));
```
Вывод:
```json
{
  "some": {
    "value":"myValue"
  }
}
```

Добавление элемента в массив объекта some.values. В этом примере будет создан новый объект. У объекта будет создано обязательно поле id.
```java
System.out.println(new MutableJson("{}").update("some.values.[testId].value", "myValue"));
```
Вывод:
```json
{
  "some": {
    "values": [
      {
        "id":"testId",
        "value":"myValue"
      }
    ]
  }
}
```

Удаление элемента из массива. Чтобы удалить элемент из массива нужно заполнить у этого объекта поле deleted со свойством true. 
```java
System.out.println(new MutableJson("{\"some\":{\"values\":[{\"id\":\"testId\",\"value\":\"myValue\"}]}}")
    .update("some.values.[testId].deleted", true));
```

```json
{
  "some": {
    "values": []
  }
}
```