package net.pervukhin.mutable.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

public class MutableJson {

    private static final String DELETED = "deleted";
    private static final String ID = "id";
    private JsonNode json;
    private ObjectMapper objectMapper = new ObjectMapper();

    public MutableJson(String jsonContent) throws IOException {
        this.json = new ObjectMapper().readValue(jsonContent, JsonNode.class);
    }

    public MutableJson update(String path,
                              Object newValue) {
        updateJson(this.json, parse(path), newValue, false);
        return this;
    }

    public MutableJson update(String path,
                              Object newValue,
                              boolean forceSetNull) {
        updateJson(this.json, parse(path), newValue, forceSetNull);
        return this;
    }

    @Override
    public String toString() {
        try {
            return objectMapper.writeValueAsString(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    private List<Token> parse(String path) {
        if (path.isEmpty()) {
            return Collections.emptyList();
        }
        if (path.startsWith("$.")) {
            path = path.substring(2);
        }

        List<Token> tokens = new ArrayList<>();
        for (String part : path.split("\\.")) {
            if (part.matches("(\\[.+\\])")) {
                String fieldName = part.substring(0, part.indexOf('['));
                String key = part.substring(part.indexOf('[') + 1, part.indexOf(']'));
                tokens.add(new ArrayToken(fieldName, key));
            } else if (part.contains("[]")) {
                String fieldName = part.substring(0, part.indexOf('['));
                tokens.add(new ArrayToken(fieldName, ""));
            } else {
                tokens.add(new FieldToken(part));
            }
        }
        return tokens;
    }

    private Boolean updateJson(JsonNode data,
                               List<Token> path,
                               Object newValue,
                               Boolean forceSetNull) {
        Token token = path.get(0);
        if (token instanceof ArrayToken) {
            ArrayToken arrayToken = (ArrayToken) token;
            JsonNode jsonNode = getByIdPath((ArrayNode) data, arrayToken.getKey());

            if (jsonNode == null) {
                if (nextElementIsArray(path)) {
                    path.remove(0);
                    ArrayNode newArray = ((ArrayNode) data).addArray();
                    updateJson(newArray, path, newValue, forceSetNull);
                } else {
                    path.remove(0);
                    ObjectNode newObject = ((ArrayNode) data).addObject();
                    newObject.put(ID, arrayToken.getKey());
                    updateJson(newObject, path, newValue, forceSetNull);
                }
            } else {
                ArrayToken previousToken = (ArrayToken) path.get(0);
                path.remove(0);
                if (updateJson(jsonNode, path, newValue, forceSetNull)) {
                    removeJsonNode((ArrayNode) data, previousToken.getKey());
                }
            }
        } else {
            if (path.size() > 1) {
                if (data.path(token.getFieldName()).isMissingNode()) {
                    if (nextElementIsArray(path)) {
                        ((ObjectNode) data).putArray(token.getFieldName());
                    } else {
                        ((ObjectNode) data).putObject(token.getFieldName());
                    }
                }
                path.remove(0);
                updateJson(data.path(token.getFieldName()), path, newValue, forceSetNull);
            } else {
                if (!DELETED.equals(token.fieldName)) {
                    if (data instanceof ObjectNode) {
                        if (newValue != null) {
                            setValue((ObjectNode) data, token.getFieldName(), newValue);
                        } else {
                            if (isValueNull(data, token.getFieldName()) || forceSetNull) {
                                setValue((ObjectNode) data, token.getFieldName(), null);
                            }
                        }
                    }
                } else {
                    return true;
                }
            }
        }
        return false;
    }

    private void removeJsonNode(ArrayNode data,
                                String key) {
        int index = 0;
        boolean found = false;
        for (Iterator<JsonNode> it = data.elements(); it.hasNext(); ) {
            JsonNode jsonNode = it.next();
            if (jsonNode.path(ID) != null && !jsonNode.path(ID).isMissingNode() &&
                    key.equals(jsonNode.path(ID).textValue())) {
                found = true;
                break;
            }
            index++;
        }
        if (found) {
            data.remove(index);
        }
    }

    private boolean isValueNull(JsonNode data,
                                String fieldName) {
        return data.get(fieldName) == null || data.get(fieldName).isMissingNode();
    }

    private JsonNode getByIdPath(ArrayNode data,
                                 String key) {
        for (Iterator<JsonNode> it = data.elements(); it.hasNext(); ) {
            JsonNode jsonNode = it.next();
            if (jsonNode.path(ID) != null && !jsonNode.path(ID).isMissingNode() &&
                    key.equals(jsonNode.path(ID).textValue())) {
                return jsonNode;
            }
        }
        return null;
    }

    private boolean nextElementIsArray(List<Token> path) {
        return (path.get(1) instanceof ArrayToken);
    }

    private void setValue(ObjectNode data,
                          String fieldName,
                          Object newValue) {
        if (newValue != null) {
            if (newValue instanceof Integer) {
                data.put(fieldName, (Integer) newValue);
            } else if (newValue instanceof Long) {
                data.put(fieldName, (Long) newValue);
            } else if (newValue instanceof String) {
                data.put(fieldName, (String) newValue);
            } else if (newValue instanceof Double) {
                data.put(fieldName, (Double) newValue);
            } else if (newValue instanceof Boolean) {
                data.put(fieldName, (Boolean) newValue);
            } else {
                data.put(fieldName, newValue.toString());
            }
        } else {
            data.put(fieldName, (String) null);
        }
    }

    abstract class Token {

        private final String fieldName;

        Token(String fieldName) {
            this.fieldName = fieldName;
        }

        String getFieldName() {
            return fieldName;
        }
    }

    class FieldToken extends Token {

        FieldToken(String fieldName) {
            super(fieldName);
        }
    }

    class ArrayToken extends Token {

        private final String key;

        ArrayToken(String fieldName,
                   String key) {
            super(fieldName);
            this.key = key;
        }

        String getKey() {
            return key;
        }
    }
}
