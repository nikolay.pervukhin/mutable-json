package net.pervukhin.mutable.json;

import java.io.IOException;

public class Application {
    public static void main(String[] args) throws IOException {
        System.out.println(new MutableJson("{}").update("some.value", "myValue"));

        System.out.println(new MutableJson("{}").update("some.values.[testId].value", "myValue"));

        System.out.println(new MutableJson("{\"some\":{\"values\":[{\"id\":\"testId\",\"value\":\"myValue\"}]}}")
                .update("some.values.[testId].deleted", true));
    }
}
